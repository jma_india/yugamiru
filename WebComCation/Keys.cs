﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QlmLicenseLib;

namespace WebComCation
{
    public static class Keys
    {
        public static string ComputerKey { get; set; }
        public static string ActivationKey { get; set; }
        public static string EndDate { get; set; }
        public static int DaysLeft { get; set; }


        public static bool StoreAKey()
        {
            QlmLicense ql = new QlmLicense();
            try
            {
                ql.StoreKeys(ActivationKey, ComputerKey);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static bool? ReadKey()
        {
            bool? op = null;
            QlmLicense ql = new QlmLicense();
            try
            {
                //Testing Sample Keys (both are valid Keys) 
                string ak = "B5HZ0X0W00BG211E8Y8W391NDJTFEIW"; //Validation Key
                string ck = "V2HY0J0200BG212C8Z8T3N1KFU8ZAJZ"; // Computer Key
                ql.DefineProduct(3, "gsport", 1, 0, "DmpuTTAjm3gV4A==", "{1d683c4c-a831-40f0-8301-726f010f5ee9}");
                ql.PublicKey = "DmpuTTAjm3gV4A==";
                ql.StoreKeys(ak, ck);
                
                string readAK = string.Empty;
                string readCK = string.Empty;

                ql.ReadKeys(ref readAK, ref readCK);
                
                ql.ValidateLicenseEx(ck, "BFEBFBFF000406E3");
                //Where "BFEBFBFF000406E3" is unique computer id registered with this license (ref QLM screen shot)
                //Same ID we are using at the time of activating the license at server
                ELicenseStatus st = ql.GetStatus();

                EndDate = Utility.DateTimeToString(ql.ExpiryDate);

                DaysLeft = ql.DaysLeft;

                op = true;
            }
            catch
            {
                op = false;
            }
            return op;
        }

    }
}
