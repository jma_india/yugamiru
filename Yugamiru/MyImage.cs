﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Windows;
using System.Windows.Forms;

namespace Yugamiru
{
    public class MyImage
    {
        public byte[] m_pSnapImage_standing;   // Œ¹‰æ‘œ‚Ì‰æ‘œ(DIB)ƒf[ƒ^ —§ˆÊ
        public byte[] m_pSnapImage_kneedown;   // ÀˆÊ
        public byte[] m_pSnapImage_side;   // ‘¤–Ê
        int m_iSize;    // ƒJƒƒ‰ BufferSize
        int m_iWidth;   // ƒJƒƒ‰ Width
        int m_iHeight;  // ƒJƒƒ‰ Height

        struct _IplImage
        {
        };

        public MyImage()
        {
            m_pSnapImage_standing = null;
            m_pSnapImage_kneedown = null;
            m_pSnapImage_side = null;

            m_iSize = 0;
            m_iHeight = 0;
            m_iWidth = 0;
        }

        ~MyImage()
        {
            if (m_pSnapImage_standing != null)
            {
                //delete[] m_pSnapImage_standing;
                m_pSnapImage_standing = null;
            }
            if (m_pSnapImage_kneedown != null)
            {
                //delete[] m_pSnapImage_kneedown;
                m_pSnapImage_kneedown = null;
            }
            if (m_pSnapImage_side != null)
            {
                //delete[] m_pSnapImage_side;
                m_pSnapImage_side = null;
            }
        }

        public int GetWidth()
        {
            return m_iWidth;
        }

        public int GetHeight()
        {
            return m_iHeight;
        }

        public int GetBitCount()
        {
            return 24;
        }

        public bool CreateBmpInfo(int iSize, int iWidth, int iHeight)
        {
            m_iSize = iSize;
            m_iWidth = iWidth;
            m_iHeight = iHeight;
            return true;
        }

        public bool ClearStandingImage()
        {
            if (m_pSnapImage_standing != null)
            {
                //delete[] m_pSnapImage_standing;
                m_pSnapImage_standing = null;
            }
            return true;
        }

        public bool ClearKneedownImage()
        {
            if (m_pSnapImage_kneedown != null)
            {
                //delete[] m_pSnapImage_kneedown;
                m_pSnapImage_kneedown = null;
            }
            return true;
        }

        public bool ClearSideImage()
        {
            if (m_pSnapImage_side != null)
            {
                //delete[] m_pSnapImage_side;
                m_pSnapImage_side = null;
            }
            return true;
        }

        public void AllocStandingImage()
        {
            if (m_pSnapImage_standing == null)
            {
                m_pSnapImage_standing = new byte[1280 * 1024 * 3];
            }
            int i = 0;
            for (i = 0; i < 1280 * 1024; i++)
            {
                m_pSnapImage_standing[3 * i + 0] = 0;
                m_pSnapImage_standing[3 * i + 1] = 0;
                m_pSnapImage_standing[3 * i + 2] = 0;
            }
        }

        public void AllocKneedownImage()
        {
            if (m_pSnapImage_kneedown == null)
            {
                m_pSnapImage_kneedown = new byte[1280 * 1024 * 3];
            }
            int i = 0;
            for (i = 0; i < 1280 * 1024; i++)
            {
                m_pSnapImage_kneedown[3 * i + 0] = 0;
                m_pSnapImage_kneedown[3 * i + 1] = 0;
                m_pSnapImage_kneedown[3 * i + 2] = 0;
            }
        }

        public void AllocSideImage()
        {
            if (m_pSnapImage_side == null)
            {
                m_pSnapImage_side = new byte[1280 * 1024 * 3];
            }
            int i = 0;
            for (i = 0; i < 1280 * 1024; i++)
            {
                m_pSnapImage_side[3 * i + 0] = 0;
                m_pSnapImage_side[3 * i + 1] = 0;
                m_pSnapImage_side[3 * i + 2] = 0;
            }
        }

        public void DrawStandingImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
        {
            DrawImage(m_pSnapImage_standing, hDC, rc, mag);
        }

        void DrawKneedownImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
        {
            DrawImage(m_pSnapImage_kneedown, hDC, rc, mag);
        }

        void DrawSideImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
        {
            DrawImage(m_pSnapImage_side, hDC, rc, mag);
        }

        //‰æ‘œ‚Ì•\Ž¦iŠg‘åj
        void DrawImage(byte[] pImage, Graphics hDC, Rectangle rc, int mag)
        {
            int width = (rc.Right - rc.Left) * mag;
            int height = (rc.Bottom - rc.Top) * mag;

            /* Bitmap BMPInfo;
             BMPInfo.Size = sizeof(BITMAPINFOHEADER);
             BMPInfo.bmiHeader.biWidth = m_iWidth;
             BMPInfo.bmiHeader.biHeight = m_iHeight;
             BMPInfo.bmiHeader.biPlanes = 1;
             BMPInfo.bmiHeader.biBitCount = 24;
             BMPInfo.bmiHeader.biCompression = 0;
             BMPInfo.bmiHeader.biSizeImage = 0;
             BMPInfo.bmiHeader.biXPelsPerMeter = 0;
             BMPInfo.bmiHeader.biYPelsPerMeter = 0;
             BMPInfo.bmiHeader.biClrUsed = 0;
             BMPInfo.bmiHeader.biClrImportant = 0;

             BMPInfo.bmiColors[0].rgbBlue = 255;
             BMPInfo.bmiColors[0].rgbGreen = 255;
             BMPInfo.bmiColors[0].rgbRed = 255;
             BMPInfo.bmiColors[0].rgbReserved = 255;

             StretchDIBits(hDC,
                 0,
                 0,
                 width,
                 height,
                 0,
                 m_iHeight - 1 - (0 + m_iHeight - 1),
                 m_iWidth,
                 m_iHeight,
                 pImage, &BMPInfo, DIB_RGB_COLORS, SRCCOPY);*/
            var output = new Bitmap(m_iWidth, m_iHeight);
            var rect = new Rectangle(0, 0, width, height);
            var bmpData = output.LockBits(rect,
                ImageLockMode.ReadWrite, output.PixelFormat);
            var ptr = bmpData.Scan0;
            Marshal.Copy(pImage, 0, ptr, pImage.Length);
            output.UnlockBits(bmpData);
            Bitmap BMPInfo = output;
        }

        public byte[] GetStandingImagePointer()
        {
            return m_pSnapImage_standing;
        }

        public byte[] GetKneedownImagePointer()
        {
            return m_pSnapImage_kneedown;
        }

        public byte[] GetSideImagePointer()
        {
            return m_pSnapImage_side;
        }

     /*   
       public int CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( byte[] pchSymbolName, byte[] pbyteImage, int iWidth, int iHeight)
       {

       #if 1
           unsigned char *pbyteEncodeBuffer = new unsigned char [ iWidth * iHeight * 3 ];
           unsigned char *imageData = new unsigned char [ iWidth * iHeight * 3 ];
           int iBmpWidthStep = ( iWidth * 3 + 3 ) / 4 * 4;
           {
               int i = 0;
               for( i = 0; i < iHeight; i++ ){
                   int iBmpBaseIndex = (iHeight - 1 - i) * iBmpWidthStep;
                   int iIplBaseIndex = i * iWidth * 3;
                   int j = 0;
                   for( j = 0; j < iWidth; j ++ ){
                       imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                       imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                       imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                   }
               }
           }
           int iEncodedImageSize = jpeg_encode( imageData, iWidth, iHeight, pbyteEncodeBuffer, 55 );
           if ( imageData != NULL ){
               delete [] imageData;
               imageData = NULL;
           }
           if ( pbyteEncodeBuffer != NULL ){
               delete [] pbyteEncodeBuffer;
               pbyteEncodeBuffer = NULL;
           }
       #else
           IplImage* iplTmp = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
           if (iplTmp == NULL)
           {
               return 0;
           }
           int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
           {
               int i = 0;
               for (i = 0; i < iHeight; i++)
               {
                   int iBmpBaseIndex = (iHeight - 1 - i) * iBmpWidthStep;
                   int iIplBaseIndex = i * iplTmp->widthStep;
                   int j = 0;
                   for (j = 0; j < iWidth; j++)
                   {
                       iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                       iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                       iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                   }
               }
           }
           unsigned char* pbyteEncodeBuffer = new unsigned char[iplTmp->width * iplTmp->height * iplTmp->nChannels];
           if (pbyteEncodeBuffer == NULL)
           {
               if (iplTmp != NULL)
               {
                   cvReleaseImage(&iplTmp);
                   iplTmp = NULL;
               }
               return 0;
           }
           int iEncodedImageSize = cvexJPEGEncode(iplTmp, pbyteEncodeBuffer, 55);

           if (iplTmp != NULL)
           {
               cvReleaseImage(&iplTmp);
               iplTmp = NULL;
           }
           if (pbyteEncodeBuffer != NULL)
           {
               delete[] pbyteEncodeBuffer;
               pbyteEncodeBuffer = NULL;
           }
       #endif
           int iRet = CalcDataSizeOfImageAssignmentStatement(pchSymbolName, iEncodedImageSize);
           return iRet;
       }
*/
        public int CalcDataSizeOfStatementBlock()
        {
            int iRet = 91842; // which is a default value for default image
                              /*  iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "StandingImage", m_pSnapImage_standing, m_iWidth, m_iHeight ) + sizeof(int);
                                    iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "KneedownImage", m_pSnapImage_kneedown, m_iWidth, m_iHeight ) + sizeof(int);
                                    iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "SideImage", m_pSnapImage_side, m_iWidth, m_iHeight ) + sizeof(int);
                                    */
            return iRet;
        }

       
       

       
        /*
                Image<Bgr, Byte> DecodeJPEGImage( byte[] pbyteSrc, int iSrcImageSize)
        {
            byte[] pbyteTmpBuffer = new byte[iSrcImageSize];
                    pbyteTmpBuffer = pbyteSrc;
                    //memcpy(pbyteTmpBuffer, pbyteSrc, iSrcImageSize);
                    Image<Bgr, Byte> piplImage = cvexJPEGDecode(pbyteTmpBuffer, iSrcImageSize, false);
            if (pbyteTmpBuffer != NULL)
            {
                delete[] pbyteTmpBuffer;
                pbyteTmpBuffer = NULL;
            }
            return piplImage;
        }

        int CMyImage::SetStandingIplImage( const IplImage* piplSrc)
        {
            if (m_pSnapImage_standing != NULL)
            {
                delete[] m_pSnapImage_standing;
                m_pSnapImage_standing = NULL;
            }
            int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
            m_pSnapImage_standing = new unsigned char[iBmpWidthStep * piplSrc->height];
            if (m_pSnapImage_standing == NULL)
            {
                return 0;
            }
            int i = 0;
            for (i = 0; i < piplSrc->height; i++)
            {
                int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
                int iIplBaseIndex = i * piplSrc->widthStep;
                int j = 0;
                for (j = 0; j < piplSrc->width; j++)
                {
                    m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                }
            }
            return 1;
        }

        int CMyImage::SetKneedownIplImage( const IplImage* piplSrc)
        {
            if (m_pSnapImage_kneedown != NULL)
            {
                delete[] m_pSnapImage_kneedown;
                m_pSnapImage_kneedown = NULL;
            }
            int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
            m_pSnapImage_kneedown = new unsigned char[iBmpWidthStep * piplSrc->height];
            if (m_pSnapImage_kneedown == NULL)
            {
                return 0;
            }
            int i = 0;
            for (i = 0; i < piplSrc->height; i++)
            {
                int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
                int iIplBaseIndex = i * piplSrc->widthStep;
                int j = 0;
                for (j = 0; j < piplSrc->width; j++)
                {
                    m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                }
            }
            return 1;
        }

        int CMyImage::SetSideIplImage( const IplImage* piplSrc)
        {
            if (m_pSnapImage_side != NULL)
            {
                delete[] m_pSnapImage_side;
                m_pSnapImage_side = NULL;
            }
            int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
            m_pSnapImage_side = new unsigned char[iBmpWidthStep * piplSrc->height];
            if (m_pSnapImage_side == NULL)
            {
                return 0;
            }
            int i = 0;
            for (i = 0; i < piplSrc->height; i++)
            {
                int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
                int iIplBaseIndex = i * piplSrc->widthStep;
                int j = 0;
                for (j = 0; j < piplSrc->width; j++)
                {
                    m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                    m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
                }
            }
            return 1;
        }

        public int SetStandingJPEGImage( string pbyteSrc, int iSrcImageSize)
        {
            Image<Bgr,byte> iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
            if (iplTmp == null)
            {
                return 0;
            }
            SetStandingIplImage(iplTmp);
            //cvReleaseImage(&iplTmp);
            //iplTmp = NULL;
            return 1;
        }

        int CMyImage::SetKneedownJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
        {
            IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
            if (iplTmp == NULL)
            {
                return 0;
            }
            SetKneedownIplImage(iplTmp);
            cvReleaseImage(&iplTmp);
            iplTmp = NULL;
            return 1;
        }

        int CMyImage::SetSideJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
        {
            IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
            if (iplTmp == NULL)
            {
                return 0;
            }
            SetSideIplImage(iplTmp);
            cvReleaseImage(&iplTmp);
            iplTmp = NULL;
            return 1;
        }

        public int ExecuteImageAssignmentStatement( string pchSymbolName, int iSymbolNameLength, string pbyteImage, int iImageSize)
        {
            if (pchSymbolName == "StandingImage")
            {
                SetStandingJPEGImage(pbyteImage, iImageSize);
                return 1;
            }
            if (pchSymbolName == "KneedownImage")
            {
                SetKneedownJPEGImage(pbyteImage, iImageSize);
                return 1;
            }
            if (pchSymbolName == "SideImage")
            {
                SetSideJPEGImage(pbyteImage, iImageSize);
                return 1;
            }
            return 0;
        }
        */
        public int ExecuteIntegerAssignmentStatement(string pchSymbolName, int iSymbolNameLength, int iValue)
        {
            return 0;
        }

        public int ExecuteStringAssignmentStatement(string pchSymbolName, int iSymbolNameLength, string pchValue, int iValueLength)
        {
            return 0;
        }
        public int ExecuteImageAssignmentStatement(string pchSymbolName, int iSymbolNameLength,
            byte[] pbyteImage, int iImageSize)
        {
            /*  if (pchSymbolName == "StandingImage")
              {

                  SetStandingJPEGImage(pbyteImage, iImageSize);
                  return 1;
              }
              if (pchSymbolName == "KneedownImage")
              {
                  SetKneedownJPEGImage(pbyteImage, iImageSize);
                  return 1;
              }
              if (pchSymbolName == "SideImage")
              {
                  SetSideJPEGImage(pbyteImage, iImageSize);
                  return 1;
              }*/
            return 0;
        }
        public int SetStandingJPEGImage(byte[] pbyteSrc, int iSrcImageSize)
        {
            // m_pSnapImage_standing = Encoding.ASCII.GetBytes(pbyteSrc);
            // Open a Stream and decode a JPEG image

            /*
                        Image<Bgr, byte> iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
                        if (iplTmp == null)
                        {
                            return 0;
                        }
            */
            //SetStandingIplImage();


            return 1;
        }

        Image<Bgr, byte> DecodeJPEGImage(string pbyteSrc, int iSrcImageSize)
        {

            byte[] pbyteTmpBuffer = new byte[iSrcImageSize];
            pbyteTmpBuffer = Encoding.ASCII.GetBytes(pbyteSrc);

            Image<Bgr, byte> piplImage = cvexJPEGDecode(pbyteTmpBuffer, iSrcImageSize, false);
            return piplImage;
        }

        Image<Bgr, byte> cvexJPEGDecode(byte[] src, long ulSrcSize, bool isSwapGBR)
        {
            int iWidth = 0;
            int iHeight = 0;
            /*      
                if (jpeg_decode(src, ulSrcSize, IntPtr.Zero, ref iWidth, ref iHeight, 0, isSwapGBR) == 0)
               {
                   return null;
               }
             */
            /* IntPtr piplImage_IntPtr = CvInvoke.cvCreateImage
                 (new Size(1024, 1280), Emgu.CV.CvEnum.IplDepth.IplDepth_8U, 3);*/
            Image<Bgr, Byte> piplImage = new Image<Bgr, byte>(new Size(1024, 1280));
            // CvInvoke.cvCopy(piplImage_IntPtr, piplImage.Ptr, IntPtr.Zero);
            iWidth = 1024;
            iHeight = 1280;
            if (jpeg_decode(src,
                ulSrcSize,
                piplImage.MIplImage.ImageData,
                ref iWidth, ref iHeight,
                piplImage.MIplImage.WidthStep,
                isSwapGBR) != 0)
            {
                return null;
            }
            return piplImage;
        }
        int jpeg_decode(
    byte[] mem_src,
    long ulSrcSize,
    IntPtr mem_dst,
    ref int piWidth,
    ref int piHeight,
    int iStep,
    bool isSwapBGR)
        {

            // struct jpeg_decompress_struct cinfo;
     /*       BitMiracle.LibJpeg.Classic.jpeg_decompress_struct cinfo =
                         new BitMiracle.LibJpeg.Classic.jpeg_decompress_struct();*/

            //struct jpeg_error_mgr jerr;
    /*        BitMiracle.LibJpeg.Classic.jpeg_error_mgr jerr =
                new BitMiracle.LibJpeg.Classic.jpeg_error_mgr();*/

            //cinfo.err = jpeg_std_error( &jerr );

            //jpeg_create_decompress( &cinfo );



            //jpeg_memory_src(&cinfo, mem_src, ulSrcSize);
            MemoryStream ms = new MemoryStream(mem_src);
           // System.IO.File.WriteAllText
            //           (@"C: \Users\Meena\Desktop\WriteLine1.txt", mem_src.ToString());


         //   cinfo.jpeg_stdio_src(ms);
            //jpeg_read_header( &cinfo, TRUE );
        //    cinfo.jpeg_read_header(true);

            //jpeg_start_decompress( &cinfo );
         //   cinfo.jpeg_start_decompress();

            //int row_stride = cinfo.output_width * cinfo.output_components;
         //   int row_stride = cinfo.Output_width * cinfo.Output_components;
            //JSAMPARRAY row_buffer = (*cinfo.mem->alloc_sarray)
            //   ((j_common_ptr) & cinfo, JPOOL_IMAGE, row_stride, 1);

         //   byte[][] row_buffer = new byte[row_stride][];

            //if (piWidth != null)
            {
            //    piWidth = cinfo.Output_width;
            }
            //if (piHeight != null)
            {
            //    piHeight = cinfo.Output_height;
            }
            int y = 0;
            int y_step = 1;
/*
            if (isSwapBGR)
            {
                while (cinfo.Output_scanline < cinfo.Output_height)
                {

                    //jpeg_read_scanlines(&cinfo, row_buffer, 1);
                    cinfo.jpeg_read_scanlines(row_buffer, 1);
                    //if (mem_dst != NULL)
                    {
                        // unsigned char* 
                        byte[] src = row_buffer[0];
                        //unsigned char* 
                        //byte[] dst =  mem_dst[y * iStep];
                        uint i = 0;
                        for (i = 0; i < cinfo.Output_width; i++)
                        {
                            /* dst[0] = src[2];
                             dst[1] = src[1];
                             dst[2] = src[0];
                             dst += 3;
                             src += 3;*/
             /*           }
                    }
                    y += y_step;
                }
            }
            else
            {
                while (cinfo.Output_scanline < cinfo.Output_height)
                {

                    //jpeg_read_scanlines(&cinfo, row_buffer, 1);
                    cinfo.jpeg_read_scanlines(row_buffer, 1);
                    //if (mem_dst != NULL)
                    {
                        //unsigned char* 
                        byte[] src = row_buffer[0];
                        //unsigned char* 
                        //byte[] dst = mem_dst[y * iStep];
                        uint i = 0;
                        for (i = 0; i < cinfo.Output_width; i++)
                        {
                            /*  dst[0] = src[0];
                              dst[1] = src[1];
                              dst[2] = src[2];
                              dst += 3;
                              src += 3;*/
                     //   }
                 //   }
                    y += y_step;
             //   }
          //  }

            //jpeg_finish_decompress(&cinfo);
        //    cinfo.jpeg_finish_decompress();

            //jpeg_destroy_decompress(&cinfo);
        //    cinfo.jpeg_destroy();
            return 1;

        }


        public int SetKneedownJPEGImage(string pbyteSrc, int iSrcImageSize)
        {
            /*
             m_pSnapImage_side = new byte[1280 * 1024 * 3];

              m_pSnapImage_side = Encoding.ASCII.GetBytes(pbyteSrc);
              Image<Bgr, byte> depthImage = new Image<Bgr, byte>(1024, 1280);

              depthImage.Bytes = m_pSnapImage_side;
              */

            m_pSnapImage_side = Encoding.Unicode.GetBytes(pbyteSrc);

            MemoryStream ms = new MemoryStream(m_pSnapImage_side, 0, m_pSnapImage_side.Length);

            ms.Write(m_pSnapImage_side, 0, m_pSnapImage_side.Length);
            //JpegBitmapDecoder decoder = new JpegBitmapDecoder(ms, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            //Image image = Image.FromStream(ms, true, true);


            /*IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
             if (iplTmp == NULL)
             {
                 return 0;
             }
             SetKneedownIplImage(iplTmp);
             cvReleaseImage(&iplTmp);
             iplTmp = NULL;*/
            return 1;
        }

        public int SetSideJPEGImage(string pbyteSrc, int iSrcImageSize)
        {
            m_pSnapImage_side = Encoding.ASCII.GetBytes(pbyteSrc);

            return 1;
        }

    }
}
