﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Yugamiru
{
    public class RealValueRangeDefinitionElement
    {
        public string m_strRangeSymbol;       // ’lˆæƒVƒ“ƒ{ƒ‹.
        int m_iMinValueOperatorID;  // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        int m_iMaxValueOperatorID;  // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        double m_dMinValue;         // Å¬’l.
        double m_dMaxValue;         // Å‘å’l.
        SymbolFunc m_SymbolFunc;


        // ’lˆæƒVƒ“ƒ{ƒ‹’è‹`‚Ìƒ}ƒXƒ^[ƒf[ƒ^‚ÌƒXƒL[ƒ}.
  public RealValueRangeDefinitionElement( )
        {
            m_SymbolFunc = new SymbolFunc();
            m_strRangeSymbol = string.Empty;                            // ’lˆæƒVƒ“ƒ{ƒ‹.

            m_iMinValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_NONE;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_NONE;	// Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

    m_dMinValue = 0.0 ;                         // Å¬’l.

            m_dMaxValue = 0.0;                                // Å‘å’l.
        
        }

       public RealValueRangeDefinitionElement( RealValueRangeDefinitionElement rSrc )
        {

            m_strRangeSymbol = rSrc.m_strRangeSymbol;           // ’lˆæƒVƒ“ƒ{ƒ‹.

            m_iMinValueOperatorID = rSrc.m_iMinValueOperatorID; // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = rSrc.m_iMaxValueOperatorID; // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

            m_dMinValue = rSrc.m_dMinValue;                 // Å¬’l.

            m_dMaxValue = rSrc.m_dMaxValue;                         // Å‘å’l.
                }

       ~RealValueRangeDefinitionElement(  )
{
}
/*
CRealValueRangeDefinitionElement &CRealValueRangeDefinitionElement::operator=( const CRealValueRangeDefinitionElement &rSrc )
{
	m_strRangeSymbol		= rSrc.m_strRangeSymbol;
	m_iMinValueOperatorID	= rSrc.m_iMinValueOperatorID;	// Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
	m_iMaxValueOperatorID	= rSrc.m_iMaxValueOperatorID;	// Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
	m_dMinValue				= rSrc.m_dMinValue;				// Å¬’l.
	m_dMaxValue				= rSrc.m_dMaxValue;				// Å‘å’l.
	return *this;
}
*/
 public bool IsEqualRangeSymbol( string pchRangeSymbol) 
{
	return( m_strRangeSymbol == pchRangeSymbol );
}

public bool IsSatisfied(double dValue) 
{
	if ( m_iMinValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_GREATER)
            {
		if ( !( dValue > m_dMinValue ) ){
			return false;
		}	
	}

	if ( m_iMinValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_GREATEROREQUAL)
            {
		if ( !(dValue >= m_dMinValue ) ){
			return false;
		}
	}

	if ( m_iMaxValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_LESS)
            {
		if ( !( dValue<m_dMaxValue ) ){
			return false;
		}	
	}

	if ( m_iMaxValueOperatorID == m_SymbolFunc.COMPAREOPERATORID_LESSOREQUAL)
            {
		if ( !( dValue <= m_dMaxValue ) ){
			return false;
		}	
	}
	return true;
}

public void GetRange(ref int iMinValueOperatorID, ref int iMaxValueOperatorID, ref double dMinValue, ref double dMaxValue )
{
    iMinValueOperatorID = m_iMinValueOperatorID;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
    iMaxValueOperatorID = m_iMaxValueOperatorID;    // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
    dMinValue = m_dMinValue;                // Å¬’l.
    dMaxValue = m_dMaxValue;				// Å‘å’l.
}

public int ReadFromString(char[] lpszText)
{
    string strRangeSymbol = string.Empty;
            string strMinValueOperator = string.Empty;
            string strMinValue = string.Empty;
            string strMaxValueOperator = string.Empty;
            string strMaxValue = string.Empty;
            string strMinusInfinity = string.Empty;
            string strPlusInfinity = string.Empty;

    TextBuffer TextBuffer = new TextBuffer(lpszText);

    TextBuffer.SkipSpaceOrTab();
    if (TextBuffer.IsEOF())
    {
        return (Constants.READFROMSTRING_NULLLINE);
    }

    if (TextBuffer.ReadNewLine())
    {
        if (TextBuffer.IsEOF())
        {
            return (Constants.READFROMSTRING_NULLLINE);
        }
    }

    if (!TextBuffer.ReadSymbol(ref strRangeSymbol))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadSymbol(ref strMinValueOperator))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadRealValue(ref strMinValue))
    {
        strMinValue = string.Empty;
        if (!TextBuffer.ReadSymbol(ref strMinusInfinity))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        if (strMinusInfinity != "MINUS_INFINITY")
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadSymbol(ref strMaxValueOperator))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadRealValue(ref strMaxValue))
    {
        strMaxValue = string.Empty;
        if (!TextBuffer.ReadSymbol(ref strPlusInfinity))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        if (strPlusInfinity != "PLUS_INFINITY")
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
    }

    TextBuffer.SkipSpaceOrTab();
    TextBuffer.ReadNewLine();
    if (!TextBuffer.IsEOF())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    m_strRangeSymbol = strRangeSymbol;
    m_iMinValueOperatorID = m_SymbolFunc.GetCompareOperatorIDBySymbol(strMinValueOperator);
    if ((m_iMinValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_GREATEROREQUAL) &&
        (m_iMinValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_GREATER))
    {
        return (Constants.READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID);
    }
    m_iMaxValueOperatorID = m_SymbolFunc.GetCompareOperatorIDBySymbol(strMaxValueOperator);
    if ((m_iMaxValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_LESSOREQUAL) &&
        (m_iMaxValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_LESS))
    {
        return (Constants.READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID);
    }

    if (strMinusInfinity == "MINUS_INFINITY")
    {
        m_iMinValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_TRUE;
        m_dMinValue = 0.0;
    }
    else
    {
                if(strMinValue != string.Empty)
                m_dMinValue = Convert.ToDouble(strMinValue);//float.Parse(strMinValue);
    }

    if (strPlusInfinity == "PLUS_INFINITY")
    {
        m_iMaxValueOperatorID = m_SymbolFunc.COMPAREOPERATORID_TRUE;
        m_dMaxValue = 0.0;
    }
    else
    {
        m_dMaxValue = float.Parse(strMaxValue);
    }
    if ((m_iMinValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_TRUE) && (m_iMaxValueOperatorID != m_SymbolFunc.COMPAREOPERATORID_TRUE))
    {
        if (m_dMinValue > m_dMaxValue)
        {
            return Constants.READFROMSTRING_RANGE_INVALID;
        }
    }

    return Constants.READFROMSTRING_SYNTAX_OK;
}


    }
}
